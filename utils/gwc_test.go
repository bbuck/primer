package utils_test

import (
	"fmt"
	"os"

	. "bitbucket.org/bbuck/primer/utils"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Gwc", func() {
	var (
		testFilename = "testwc.txt"
		result       WCResult
		err          error
	)

	Context("file does not exist", func() {
		It("should fail when given an invalid file", func() {
			_, err = WordCount("hello")
			Expect(err).ToNot(BeNil())
		})
	})

	Context("file exists", func() {
		Context("empty file", func() {
			var (
				testContents = ""
				testResult   = WCResult{Words: 0, Lines: 0, Characters: 0}
			)

			BeforeEach(func() {
				f, _ := os.Create(testFilename)
				defer f.Close()
				fmt.Fprintf(f, testContents)
			})

			AfterEach(func() {
				os.Remove(testFilename)
			})

			It("should return valid result", func() {
				result, err = WordCount(testFilename)
				Expect(err).To(BeNil())
				Expect(result).To(Equal(testResult))
			})
		})

		Context("normal ASCII File", func() {
			var (
				testContents = "one two three four."
				testResult   = WCResult{Words: 4, Lines: 1, Characters: 19}
			)

			BeforeEach(func() {
				f, _ := os.Create(testFilename)
				defer f.Close()
				fmt.Fprintf(f, testContents)
			})

			AfterEach(func() {
				os.Remove(testFilename)
			})

			It("should return valid result", func() {
				result, err = WordCount(testFilename)
				Expect(err).To(BeNil())
				Expect(result).To(Equal(testResult))
			})
		})

		Context("normal unicode file", func() {
			var (
				testContents = "こんにちわ"
				testResult   = WCResult{Words: 1, Lines: 1, Characters: 5}
			)

			BeforeEach(func() {
				f, _ := os.Create(testFilename)
				defer f.Close()
				fmt.Fprintf(f, testContents)
			})

			AfterEach(func() {
				os.Remove(testFilename)
			})

			It("should return valid result", func() {
				result, err = WordCount(testFilename)
				Expect(err).To(BeNil())
				Expect(result).To(Equal(testResult))
			})
		})
	})
})
