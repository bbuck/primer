package utils

import (
	"bufio"
	"errors"
	"os"
)

// LineProcessor does things.
type LineProcessor func(string)

// ReadLines does things.
func ReadLines(filename string, processor LineProcessor) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	fi, err := file.Stat()
	if err != nil {
		return err
	}

	switch mode := fi.Mode(); {
	case mode.IsDir():
		return errors.New("Cannot process directories.")
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		processor(scanner.Text())
	}

	return nil
}
