package utils_test

import (
	. "bitbucket.org/bbuck/primer/utils"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("ReadLines", func() {
	var (
		dummyProcessor LineProcessor = func(string) {}
		err            error
	)
	It("should error when given an invalid file name", func() {
		err = ReadLines("hello", dummyProcessor)
		Expect(err).ToNot(BeNil())
	})

	It("should not fail reading valid files", func() {
		err = ReadLines("gcat.go", dummyProcessor)
		Expect(err).To(BeNil())
	})

	It("should fail when not reading a file", func() {
		err = ReadLines("../utils", dummyProcessor)
		Expect(err).ToNot(BeNil())
	})
})
