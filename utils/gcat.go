package utils

import (
	"fmt"
	"io"
)

// CatFile takes a file and output source and then
// reads the contents of the file and prints them
// to the output source.
func CatFile(filename string, outputSource io.Writer) error {
	err := ReadLines(filename, func(line string) {
		fmt.Fprintln(outputSource, line)
	})

	return err
}
