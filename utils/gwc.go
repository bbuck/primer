package utils

import "unicode"

// WCResult does things.
type WCResult struct {
	Words, Characters, Lines int
}

// WordCount does things
func WordCount(filename string) (result WCResult, err error) {
	seenWhitespace := false
	err = ReadLines(filename, func(line string) {
		result.Lines++
		for _, c := range line {
			result.Characters++
			if unicode.IsSpace(c) {
				seenWhitespace = true
			} else if seenWhitespace {
				seenWhitespace = false
				result.Words++
			}
		}

		if !seenWhitespace {
			result.Words++
		}
	})

	return
}
