package utils_test

import (
	"bytes"
	"io/ioutil"

	. "bitbucket.org/bbuck/primer/utils"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("CatFile Function", func() {
	var (
		err error
		buf *bytes.Buffer
	)

	BeforeEach(func() {
		err = nil
		buf = new(bytes.Buffer)
	})

	It("should error when given an invalid file name", func() {
		err = CatFile("hello", buf)
		Expect(err).ToNot(BeNil())
	})

	It("should not fail reading valid files", func() {
		err = CatFile("gcat.go", buf)
		Expect(err).To(BeNil())
	})

	It("should fail when not reading a file", func() {
		err = CatFile("../utils", buf)
		Expect(err).ToNot(BeNil())
	})

	It("should read file contents and write to buffer", func() {
		fileContents, _ := ioutil.ReadFile("gcat.go")
		err = CatFile("gcat.go", buf)
		Expect(err).To(BeNil())
		Expect(fileContents).To(Equal(buf.Bytes()))
	})
})
