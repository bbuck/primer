package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Fprintf(os.Stderr, "No filename given to cat!\n")
		os.Exit(1)
	}

	filename := os.Args[1]

	utils.WordCount(filename)
}
