package main

import (
	"fmt"
	"os"

	"bitbucket.org/bbuck/primer/utils"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Fprintf(os.Stderr, "No filename given to cat!\n")
		os.Exit(1)
	}

	filename := os.Args[1]

	err := utils.CatFile(filename, os.Stdout)
	panic(err)
}
