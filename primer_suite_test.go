package primer_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestPrimer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Primer Suite")
}
